import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson2',
  templateUrl: './lesson2.component.html',
  styleUrls: ['./lesson2.component.css']
})
export class Lesson2Component implements OnInit {

  number:number=60
  ispass:boolean=true;
  constructor() { }

  ngOnInit() {
  }

  clickme(cal) {
    alert(cal*100);
  }

}
