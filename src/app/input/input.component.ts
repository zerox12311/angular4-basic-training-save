import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  // templateUrl: './input.component.html',
  template:`
  <p>{{id}}:{{name}}</p>
  `,
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() id: string;
  @Input('userName') name: string;    //with property binding

  constructor() { }

  ngOnInit() {
  }

}
